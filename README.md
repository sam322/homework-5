# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp
(λp.pz) ()λq.w) (λw.wqzp)
2. λp.pq λp.qp
(λp.pq) (λp.qp)

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs1.s1 z λq.s2 q
z and q are free variables. both S(broken down into s1 and s2 are bound) s1 is bound to λs and s2 is bound to λq

2. (λs. s z) λq. w λw. w q z s
s and z are free variables along with w q and z

3. (λs.s1) (λq.qs2)
s2 is free.  S1 is bound. q is bound to λq

4. λz. (((λs.sq) (λq.qz)) λz. (z z))

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)

(((λz.z)((λq.q) q))((λs.s) a))

(((λz.z)((λq.q) q1))((λs.s) a))

(((λz.z)((q)[q-->q1]))((s)[s-->a]))

((λz.z)(q1)(a))

((z)([z-->q1])(a))

((q1) (a))
q1 a

2. (λz.z) (λz.z z) (λz.z q)

(((λz.z)(λz.z z))(λz.z q))

(((λa.a)((λb.b) c))(λd.d q))
(b)[b-->c]

(((λa.a)c)(λd.d q))

(((λa.a)c)(λd.d q))

(((a)[a-->c])(λd.d q))

(c(λd.d q))

(c(d)[d-->q))
cq

3. (λs.λq.s q q) (λa.a) b

(((((λs.λq.s) q1) q2))(λa.a)b)

(((((λq.s)[s-->q1] q2))(a)[a-->b])

((((λq.q1)q2))(b))

((((λq.q1)q2))(b))

(((q1)[q-->q2])(b))

(q1)(b)

q1b

4. (λs.λq.s q q) (λq.q) q

((((λs.λq.s)q1)q2)(λq3.q3)q4)

((((λq.s)[s-->q1])q2)(q3)[q3->q4])

(((λq.q1)q2)(q4))

(((λq.q1)[q-->q2])(q4))

q1q4

5. ((λs.s s) (λq.q)) (λq.q)   

((λs.s)r)(λq.q))(λa.a))

((s)[s-->r])(λq.q))(λa.a))

((r)(λq.q))(λa.a))

((r)(λq.q))(λa.a))

## Question 4

1. Write the truth table for the or operator below.
Truth Table OR
TF T
FF F
TT T
FT T


2. The Church encoding for OR = (λpq.p p q)
Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions.

OR            T     T
(λpq.ppq)(λaλb.a)(λaλb.a)
(λq.ppq)([p==>λaλb.a])(λaλb.a)
(λq.λaλb.aλaλb.aq)(λaλb.a)
(λaλb.aλaλb.aq)[q-->(λaλb.a)]
(λaλb.a)(λaλb.a)(λaλb.a)
T

(λpq.p p q)(T)(F)

(λpq.ppq)(λaλb.a)(λaλb.b)
(λq.ppq)([q==>λaλb.a])(λaλb.a)
(λq.λaλb.aλaλb.aq)(λaλb.a)
((λaλb.a)(λaλb.a)q)([q-->λaλb.a])
((λaλb.a)(λaλb.a)(λaλb.a)
(λaλb.a) is True

(λpq.p p q)(F)(F)

(λq.FFq)(p==>F)(F)
(λq.FFq)(F)
(FFq)(q==>F)
(FFF)
F


(λpq.p p q)(F)(T)

(λq.FFq)(p==>F)(F)
(λq.FFq)(T)
(FFq)(q==>F)
(FFT)
T













## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

NOT Table
T--> F
F--> T

The truth table is pretty similar to the OR table(Above) with one main diffrence(TT-->F in =!)



NOT (λx.x(λuv.v)(λab.a))

 (λx.xFT) (T)
  (xFT)[x-->(T)]
  TFT
  F

(λx.xFT) (F)
 FFT
 T


## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user).
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.
